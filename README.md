https://tjysdsg.github.io

# ML/AI (mainly speech processing)

- Contributing to [Espnet](https://github.com/espnet/espnet)
- Computer Aided Pronunciation Training (CAPT) [server backend](https://github.com/tjysdsg/capt-public)
- CAPT mobile app called [hippo](https://github.com/tjysdsg/hippo)
- [Kaldi fork](https://github.com/tjysdsg/kaldi) for CAPT using Goodness of Pronunciation (GOP)
- Standard Mandarin (no accent) acoustic model [training script](https://github.com/tjysdsg/std-mandarin-kaldi)
- Mandarin Tone Classification [experiments](https://github.com/tjysdsg/tone_classifier)
- Phone-level force alignment [scripts](https://github.com/tjysdsg/kaldi-align-to-phones) using Kaldi
- Phone-level force alignment [scripts](https://github.com/tjysdsg/aidatatang_force_align) for my paper
- Hand-written [speech recognition system](https://github.com/tjysdsg/speech-recognition) for English pronunciation of
  digits using Python+Numpy
- Implementation of some fundamental ML algorithms: https://github.com/tjysdsg/ml
- [Dance style classifier](https://github.com/tjysdsg/dance-classifier)
- Old pytorch ASR experiments https://github.com/tjysdsg/pytorch-projects

# Compiler

- https://github.com/tjysdsg/tan

# OS

- https://github.com/tjysdsg/tos
- https://github.com/tjysdsg/newlib
- https://github.com/tjysdsg/acpica

# Game Dev

- My Victoria 3 mods https://github.com/tjysdsg/tjy_vic3_fix
- Steam profile https://steamcommunity.com/id/tjysxsg/
- Dynamically generate road mesh like in city building game https://github.com/tjysdsg/dynamic_road_gen

# Tools, Experiments, and Misc

- [Prebuilt LLVM and Clang libraries](https://github.com/tjysdsg/llvm-build)
- Chinese endemic bird image dataset https://github.com/tjysdsg/birds
- [Prebuilt Openpose](https://github.com/tjysdsg/openpose-built)
- Sorting algorithm test benchmarks https://github.com/tjysdsg/sort-test-bench
- Float representation tool https://github.com/tjysdsg/float_repr
- Prebuilt cross compiling gnu toolchains https://github.com/tjysdsg/cross-gnu
- Extract phone alignment from Kaldi kaldi ali.*.gz https://github.com/tjysdsg/ali_to_phone
- Misc scripts https://github.com/tjysdsg/scripts
- Experimenting different parallel computing methods https://github.com/tjysdsg/parallel_cpp
- Neovim configs https://github.com/tjysdsg/nvim
- Calculate WER https://github.com/tjysdsg/wer
- Easily bootstrapping a new Ubuntu WSL installation https://github.com/tjysdsg/wsl-bootstrap

# 3D Modeling

- https://github.com/tjysdsg/blender-projects
